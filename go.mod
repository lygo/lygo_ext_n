module bitbucket.org/lygo/lygo_ext_n

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.44
	bitbucket.org/lygo/lygo_events v0.1.5
	bitbucket.org/lygo/lygo_ext_dbbolt v0.1.2
	bitbucket.org/lygo/lygo_ext_http v0.1.12
	bitbucket.org/lygo/lygo_ext_logs v0.1.4
	bitbucket.org/lygo/lygo_nio v0.1.4
	github.com/gofiber/fiber/v2 v2.2.4
	github.com/google/uuid v1.1.2 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/valyala/fasthttp v1.18.0 // indirect
	golang.org/x/sys v0.0.0-20201207223542-d4d67f95c62d // indirect
)
